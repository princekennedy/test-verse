<?php

namespace App\Filament\Pages;

use Filament\Pages\Page;
use Filament\Resources\Form;
use Filament\Forms\Components\Section;
use Illuminate\Contracts\View\View;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;


class ImageOptimizer extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.pages.image-optimizer';
}
