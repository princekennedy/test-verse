<?php

namespace App\Cache;

// use Orhanerday\OpenAi\OpenAi;
use OpenAI;
use Illuminate\Support\Facades\Http;

/**
 * 
 */
class OpenAICache
{

    public function getSearchOpenAI($text = null){
        // $open_ai = new OpenAi( env('OPENAI_API_KEY') );
        $open_ai = OpenAI::client( env('OPENAI_API_KEY') );
        $search = sprintf( request()->search ?? "Hello Friend");
        $search = $text ? $text : $search;
        $search = str_contains($search, "?") ? $search : $search . " ?";

        if( $text) {
        } else {
            $search = $search . " and format result with html tags and add br tag where neccesary";
        }

        $search = $search . " : Maximum words 4096";

        // $search = $search . " and format result in html tags with neccesary style add br tag where necessary and wrapper content in editor like html style with dark background where necessary ";

        // < with   &lt;  or &60; and >   with  &gt; or &62; 
// ( if is result contains computer code except html wrapper content in dark html editor style where necessary and  replacing < with   &60;  and >   with  &62; only)
        $result = $open_ai->completions()->create([
            // "model" => "text-davinci-003",
            "model" => "gpt-4",
            "temperature" => 0.7,
            "top_p" => 1,
            "frequency_penalty" => 0,
            "presence_penalty" => 0,
            'max_tokens' => 600,
            'prompt' => $search,
        ]);
        return $result;
    }

    public function getModels(){
        $data = [
            [ 
                "name" => "text-davinci-002",
                "description" => "A general-purpose model for text generation and understanding."
            ],
            [ 
                "name" => "text-curie-001" ,
                "description" => "A model that specializes in answering questions with a high level of accuracy."
            ],
            [ 
                "name" => "text-babbage-001",
                "description" => "A model that specializes in understanding and generating code."
            ],
            [ 
                "name" => "text-bart-001" ,
                "description" => "A model that specializes in text summarization."
            ],
            [ 
                "name" => "text-curious-001",
                "description" => "A model that specializes in understanding and answering questions about text."
            ],
            [ 
                "name" => "text-musk-002",
                "description" => "A model that specializes in creative writing and poetry."
            ],
            [ 
                "name" => "text-babbage-001",
                "description" => "A model that specializes in understanding and generating code."
            ],
            [ 
                "name" => "image-alpha-001",
                "description" => "A model that specializes in image generation and editing."
            ],
            [ 
                "name" => "davinci-codex-001",
                "description" =>"A more powerful version of text-davinci-002, which can generate more sophisticated and human-like text"
            ],    
            [ 
                "name" => "text-davinci-003",
                "description" =>"A more powerful version Latest In Trial"
            ]
        ];
        return $data;
    }


    public function openaiAuth(){
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . env('OPENAI_API_KEY'),
        ];
        return Http::withHeaders($headers)
            ->withUrlParameters([
                'endpoint' => env('OPENAI_API_URL'),
                "model" => "gpt-4",
                // "model" => "text-davinci-003",

            ]);
    }

    public function processResponse($response) {
        $status = null;
        if($response->ok()){
            return $response->json();
        }
        return $status;
    }


    public function openaiSearch( $search = null ){
        $search = $search ?? request()->search;
        $body = [
            'model' => 'gpt-4',
            // 'model' => 'gpt-3.5-turbo',
            'messages' => [
                [
                    'role' => 'user',
                    'content' => $search,
                ]
            ]
        ];

        $http = $this->openaiAuth()->post("{+endpoint}/chat/completions", $body);
        return $this->processResponse($http);
    }

    public function openaiFixSpelling( $search = null ){
        $search = $search ?? request()->search;
        $body = [
            'model' => 'text-davinci-edit-001',
            "input" => $search,
            "instruction" => "Fix the spelling mistakes",
        ];

        $http = $this->openaiAuth()->post("{+endpoint}/edits", $body);
        return $this->processResponse($http);
    }

    public function openaiImageGenerates( $search = null ){
        $search = $search ?? request()->search;
        $body = [
            "prompt" => $search,
            "n"      => request()->num ?? 3,
            "size"   => request()->size ?? "1024x1024",
        ];

        $http = $this->openaiAuth()->post("{+endpoint}/images/generations", $body);
        return $this->processResponse($http);
    }

    public function openaiModerations( $search = null ){
        $search = $search ?? request()->search;
        $body = [
            "input" => $search,
        ];

        $http = $this->openaiAuth()->post("{+endpoint}/moderations", $body);
        return $this->processResponse($http);
    }

}