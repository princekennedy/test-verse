<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use App\Models\Scopes\Searchable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Filament\Models\Contracts\FilamentUser;
use JeffGreco13\FilamentBreezy\Traits\TwoFactorAuthenticatable;


class User extends Authenticatable implements HasMedia
{
    use HasRoles;
    use Notifiable;
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use HasApiTokens;
    use InteractsWithMedia;

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'created_by',
        'updated_by',
    ];
 
    public function canAccessFilament(): bool
    {
        return true; //$this->isSuperAdmin();
    }

    protected $searchableFields = ['*'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['image'];

    public function getImageAttribute(){
        $file = $this->file();
        return $file ? $file->getUrl() : '/img/human.png';
    }

    public function file($collection = "image"){
        return $this->getMedia($collection)->last();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }


    public function optimises()
    {
        return $this->morphMany(Optimise::class, 'optimisable');
    }
    
    public function users2()
    {
        return $this->hasMany(User::class, 'updated_by');
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole('super-admin');
    }
}
