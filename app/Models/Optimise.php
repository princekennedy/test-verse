<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use App\Models\Scopes\Searchable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


class Optimise extends Model implements HasMedia
{
    use Notifiable;
    use Searchable;
    use SoftDeletes;
    use InteractsWithMedia;

    protected $fillable = [
        'name',
        'optimisable_id',
        'optimisable_type',
        'created_by',
        'updated_by',
    ];
 
    protected $searchableFields = ['*'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = [
    ];

    protected $appends = ['image', 'optimised'];

    public function getImageAttribute(){
        $file = $this->file();
        return $file ? $file->getUrl('image') : null;
    }

    public function file($collection = "optimiser"){
        return $this->getMedia($collection)->last();
    }

    public function getOptimisedAttribute(){
        $file = $this->file();
        return $file ? $file->getUrl('optimised') : null;
    }


    public function optimisable()
    {
        return $this->morphTo();
    }

    public function registerMediaConversions(Media $media = null) : void
    {
        if( request()->max_width ) {
            $this->addMediaConversion('optimised')->width( request()->max_width )->height(  request()->max_width  );
            // ->crop('crop-center', 600, 400); // Trim or crop the image to the center for specified width and height.
        } else {
            $this->addMediaConversion('optimised')->width( 1500 )->height(  1500  );
        }

        // $this->addMediaConversion('cropped')->fit(Manipulations::FIT_CROP, 600, 400);

        $this->addMediaConversion('thumb')
              ->width(300)
              ->height(300)
              ->sharpen(10);        
        // $this->addMediaConversion('thumb_large')
        //       ->width(600)
        //       ->height(500)
        //       ->sharpen(10);
    
        $this->addMediaConversion('image')
              ->width(1000);
              // ->height(400);

    }


}
