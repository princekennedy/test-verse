<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class DataIndex extends Model implements HasMedia
{
    
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name',
        'content',
        'description',
        'created_by',
        'updated_by',
    ];




}
