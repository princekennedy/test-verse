<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;
use App\Models\ActiveConfig;
use App\Models\Permission;

trait ActiveConfigTrait
{

    public static function bootActiveConfigTrait(){}

    public static function setConfig($params, $remove = null){
    	if($remove) self::deleteConfig($remove);
        $params['user_id'] = auth()->id();
        $activeConfig = auth()->user()->activeConfig()->where('key', _from($params, 'key'))->first();
        ($activeConfig) ? $activeConfig->update($params) : ActiveConfig::create($params);
    }

    public static function setGlobal($params, $remove = null){
        if($remove) self::deleteConfig($remove);
        $activeConfig = ActiveConfig::where('key', _from($params, 'key') )->first();
        ($activeConfig) ? $activeConfig->update($params) : ActiveConfig::create($params);
    }

    public static function getGlobal($key){
        if(is_string($key)) return ActiveConfig::where('key' , $key)->value('value');
        if(is_array($key)) return ActiveConfig::whereIn('key' , $key)->get('value');
    }

    public static function getGlobalInstance($key){
        if(is_string($key)) return ActiveConfig::where('key' , $key)->first();
        if(is_array($key)) return ActiveConfig::whereIn('key' , $key)->get('value');
    }

    public static function getConfig($key){
        if(is_string($key)) return auth()->user()->activeConfig()->where('key' , $key)->value('value');
        if(is_array($key)) return auth()->user()->activeConfig()->whereIn('key' , $key)->get('value');
    }

    public static function ____($key){
        if(is_string($key)) return auth()->user()->activeConfig()->where('key' , $key )->where('school_id', auth()->user()->getConfig('school_id') )->value('value');
        if(is_array($key)) return auth()->user()->activeConfig()->whereIn('key' , $key )->where('school_id', auth()->user()->getConfig('school_id') )->get('value');
    }

    public static function deleteConfig($key){
        if(is_string($key)) return auth()->user()->activeConfig()->where('key' , $key)->delete();
        if(is_array($key)) return auth()->user()->activeConfig()->whereIn('key' , $key)->delete();
    }

    // public static function permissions(){
    //     $permissionList = [];
    //     $roles = auth()->user()->roles;
    //     foreach ($roles as $key => $role) {
    //         $newList = $role->permissions()->pluck('name')->toArray();
    //         $permissionList = array_merge($permissionList, $newList);
    //     }
    //     return $permissionList;
    // }

}
