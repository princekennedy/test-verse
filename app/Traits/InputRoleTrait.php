<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;

trait InputRoleTrait
{


    public static function bootInputRoleTrait()
    {

        if (auth()->check() && !auth()->user()->isSuperAdmin()) {

            static::addGlobalScope(function (Builder $builder) {
                // $builder->whereHas('roles', function($q) {
                //     $q->whereIn('id', auth()->user()->roles);
                // }); //->orWhereNull($field);
            });            

      //       static::creating(function ($model) {
		    //     $model->created_by = auth()->id();
		    // });

      //       static::updating(function ($model) {
		    //     $model->updated_by = auth()->id();
		    // });

      //       static::deleting(function ($model) {
      //           $model->updated_by = auth()->id();
      //       });

        }
    }

}
