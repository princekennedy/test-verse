<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;

trait BusinessTypeTrait
{


    public static function bootBusinessTypeTrait()
    {

	    if (auth()->check()) {

            static::addGlobalScope(function (Builder $builder) {
                $builder->where('business_type_id', auth()->user()->business_type_id ); //->orWhereNull($field);
            });            

            static::creating(function ($model) {
		        $model->business_type_id = auth()->user()->business_type_id;
		    });

            static::updating(function ($model) {
		        $model->business_type_id = auth()->user()->business_type_id;
		    });

            static::deleting(function ($model) {
                $model->business_type_id = auth()->user()->business_type_id;
            });

        }
    }

}
