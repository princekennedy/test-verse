<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;

trait BranchTrait
{


    public static function bootBranchTrait()
    {
        

        if (auth()->check() ) {

            static::addGlobalScope(function (Builder $builder) {
            	if( ! request()->is('transfers*') ){
                    $branch_id = request()->branch_id ??  auth()->user()->branch_id;
                    $builder->where("branch_id", $branch_id );
                }

            });            

            static::creating(function ($model) {
                $branch_id = request()->branch_id ??  auth()->user()->branch_id;
		        $model->branch_id = $branch_id;
		    });

      //       static::updating(function ($model) {
		    //     $model->updated_by = auth()->id();
		    // });

      //       static::deleting(function ($model) {
      //           $model->updated_by = auth()->id();
      //       });

        }
    }

}
