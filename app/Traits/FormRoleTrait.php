<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;

  
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
  

trait FormRoleTrait
{


    public static function bootFormRoleTrait()
    {

        if (auth()->check() && !auth()->user()->isSuperAdmin()) {

            static::addGlobalScope(function (Builder $builder) {
                $builder->whereHas('roles', function($q) {
                    $q->whereIn('id', auth()->user()->roles->pluck('id'));
                }); //->orWhereNull($field);
            });            


      //       static::creating(function ($model) {
		    //     $model->created_by = auth()->id();
		    // });

      //       static::updating(function ($model) {
		    //     $model->updated_by = auth()->id();
		    // });

      //       static::deleting(function ($model) {
      //           $model->updated_by = auth()->id();
      //       });

        }
    }
    

}
