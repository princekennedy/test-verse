<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Filament\Resources\RoleResource;
use App\Filament\Resources\PermissionResource;
use App\Filament\Resources\UserResource;
use Filament\Facades\Filament;
use Filament\Navigation\NavigationBuilder;
use Filament\Navigation\NavigationGroup;
use Filament\Navigation\NavigationItem;
use Filament\Navigation\UserMenuItem;

class FilamentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

         
        // Filament::serving(function () {
        //     Filament::registerUserMenuItems([
        //         'account' => null,
        //         // ...
        //     ]);
        // });

        // Filament::navigation( function (NavigationBuilder $builder) : NavigationBuilder {
        //     return $builder->items([
        //             NavigationItem::make('Dashboard')
        //             ->icon('heroicon-o-home')
        //             ->activeIcon('heroicon-s-home')
        //             ->isActiveWhen(fn (): bool => request()->routeIs('filament.pages.dashboard'))
        //             ->url(route('filament.pages.dashboard')),
        //         ])->groups(FilamentServiceProvider::menuHandler());
        // });

    }

    public static function menuHandler() {
        $menu = [
            NavigationGroup::make('Modules')->items([
                ...\App\Filament\Pages\ImageOptimizer::getNavigationItems(),
                ...\App\Filament\Pages\Assistant::getNavigationItems(),
                // NavigationItem::make()->label('Assistant')->url('/assistances')->icon('heroicon-s-document-text'),
                // NavigationItem::make()->label('Voice To Text')->url('/voice-to-text')->icon('heroicon-s-document-text'),
                NavigationItem::make()->label('Chat')->url('/chat')->icon('heroicon-s-chat'),
                
            ]),
        ];

        if( auth()->check() ) {

            if( auth()->user()->isSuperAdmin() ) {

                $menu[] = NavigationGroup::make('User Management')->items([
                        ...UserResource::getNavigationItems(),
                        ...RoleResource::getNavigationItems(),
                        ...PermissionResource::getNavigationItems(),
                    ]);

            }

        }

        $menu[] =  NavigationGroup::make('About Us')->items([
                NavigationItem::make()->label('About')->url('/admin/about')->icon('heroicon-s-document-text'),
            ]);
        return $menu;
 
    }
}
