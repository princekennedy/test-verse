<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Carbon\Carbon;
use Facades\App\Cache\OpenAICache;

class Assistant extends Component
{

    public $commands = [
        'search' => "Chat",
        ''
    ];
    public $messages = [];
    
    public $command = 'search';


    public $result   = null;
    public $search   = null;

    public function render()
    {
        return view('livewire.assistant');
    }

    public function submit(){

        if ( ! $this->search ) return;
        $this->messages[] = [
            "name" => "me",
            "text" => $this->search,
            "result" => null,
            "dateTime" => Carbon::now(),
            "me" => true,
        ];

        if( $this->command == "search") {

            try{
                $result  = OpenAICache::openaiSearch( $this->search );
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');

        } elseif( $this->command == 'bible-question') {

            try{
                $result  = OpenAICache::openaiSearch( $this->search . " from bible ");
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');

        } elseif( $this->command == 'fix-spelling') {

            try{
                $result  = OpenAICache::openaiFixSpelling( $this->search );
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'text');

        } elseif( $this->command == 'image-generations') {

            try{
                $result  = OpenAICache::openaiImageGenerates( $this->search );
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'text');

        } elseif( $this->command == 'moderations') {

            try{
                $result  = OpenAICache::openaiModerations( $this->search );
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');

        } else {

            try{
                $result  = OpenAICache::openaiSearch( $this->search );
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');

        }
        $text = str_replace("`", "<br>", $text);
        $text = str_replace("\n", "<br>", $text);
        $this->search = null;
        $this->messages[] = [
            "name" => "Machine",
            "text" => $text,
            "result" => $result,
            "dateTime" => Carbon::now(),
            "me" => false,
        ];
        // $this->dispatchBrowserEvent('scrollToBottom', ['id' =>'messages']);

    }
}
