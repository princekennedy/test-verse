<?php
namespace App\Http\Livewire;

use Livewire\Component;
use Filament\Forms;
use Illuminate\Contracts\View\View;
use App\Models\Optimise;
use App\Models\User;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Tables\Actions\Action;
use Illuminate\Support\Facades\Http;


class ImageOptimiser extends Component implements Forms\Contracts\HasForms
{

    use Forms\Concerns\InteractsWithForms;
 
    public $fileName;

    public $size;

    public $image;

    public $result;

    public $optimiser;

    public $isLoading = false;
    
    protected $listeners = ['download'];

    public function mount(): void
    {
        // $this->form->fill([
        //     'title' => $this->user->name,
        // ]);


    }
 
    protected function getFormSchema(): array 
    {
        return [
            SpatieMediaLibraryFileUpload::make('image')->collection('image'),
            Forms\Components\TextInput::make('size')->label('Width (Default 1000)')->numeric()->minValue(10)->maxValue(100000)->required(),
            // FileUpload::make('image')->preserveFilenames(),
            // Forms\Components\TextInput::make('Submit'),
        ];
    } 
 
    public function submit(): void
    {


        $this->isLoading = true;
        request()->merge(["max_width" => $this->size ]);
        if($this->image) {
            $this->optimiser = auth()->user()->optimises()->first();
            if( ! $this->optimiser ) {
                $this->optimiser = auth()->user()->optimises()->create([]);
            }
            $this->optimiser->clearMediaCollection('optimiser');
            foreach ($this->image as $file) {
                if($file) $this->optimiser->addMedia($file)->toMediaCollection("optimiser");
            }
        }

        // $file = null;
        // $this->isLoading = true;
        // foreach ($this->image as $image) {
        //     $file = $image;
        // }

        // $this->fileName = $file->getClientOriginalName();
        // $response = Http::timeout(600)
        //     ->attach('files',file_get_contents($file->getRealPath()) , $this->fileName)
        //     ->post('http://api.resmush.it/?qlty=50');

        // if ($response->successful()) {
        //     $this->result = $response->json();
        // } else {
        //     $this->result = $response->serverError() ? 'Server Error' : 'Client Error';
        // }

        $this->image = false;
        $this->isLoading = false;
    }

    public function download($url)
    {
        return response()->streamDownload(function () use ($url) {
            echo file_get_contents($url);
        }, $this->optimiser->file()->file_name );
    }
 
    public function render()
    {
        return view('livewire.image-optimiser');
    }

}

