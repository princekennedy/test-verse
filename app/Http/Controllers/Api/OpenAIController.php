<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Facades\App\Cache\OpenAICache;
use App\Models\ChMessage;

use Illuminate\Support\Facades\Http;

// use CodeDredd\Soap\Facades\Soap;
use RicorocksDigitalAgency\Soap\Facades\Soap;


class OpenAIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if( request()->ajax() ) {

            $result  = OpenAICache::openaiSearch();
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');
            $text = str_replace("`", "<br>", $text);

            return response([
                "text" => $text,
                "result"  => $result,                
            ]);
                
            // $result  = OpenAICache::getSearchOpenAI();
            // $text = trim($result['choices'][0]['text']);

            // return response([
            //     "text" => $text,
            //     "result"  => $result,                
            // ]);

        }

        return view('openai');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($command)
    {

        if( $command == "search") {

            try{
                $result  = OpenAICache::openaiSearch();
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');

        } elseif( $command == 'bible-question') {

            try{
                $result  = OpenAICache::openaiSearch(request()->search . " from bible ");
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');

        } elseif( $command == 'fix-spelling') {

            try{
                $result  = OpenAICache::openaiFixSpelling();
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'text');

        } elseif( $command == 'image-generations') {

            try{
                $result  = OpenAICache::openaiImageGenerates();
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'text');

        } elseif( $command == 'moderations') {

            try{
                $result  = OpenAICache::openaiModerations();
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');

        } else {

            try{
                $result  = OpenAICache::openaiSearch();
            } catch (\Throwable $e) { $result = null; }
            $text = _from($result, 'choices');
            $text = _from($text, 0);
            $text = _from($text, 'message');
            $text = _from($text, 'content');

        }

        $text = str_replace("`", "<br>", $text);
        $text = str_replace("\n", "<br>", $text);

        // if( ! $result ) {
        //     return response(['error' => "Something went wrong, please re-sent message."]);
        // }

        return response([
            "command" => $command,
            "text" => $text,
            "result"  => $result,                
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function gpt(Request $request) {

        return view('gpt');
    }

    public function imageGenerators(Request $request) {
        $result  = OpenAICache::openaiImageGenerates();
        $text = _from($result, 'choices');
        $text = _from($text, 0);
        $text = _from($text, 'text');
        return response([
            "text" => $text,
            "result" => $result,
        ]);
    }


    public function test2(){
        // try{
        //     // $response = Soap::to('https://fleetmw.cartrack.com:443/api/index.php?wsdl')
        //     //         ->withBasicAuth('MALA00011', 'LIVERPOOL1//');

        //     $response = Soap::fake(['http://endpoint.com:Details|Information|Overview' => Response(['foo' => 'bar'])])->throw();
        // } catch (\Throwable $e) {
        //     dd($e);
        // }

        // if($response->ok()){
        //     return $response->json();
        // } else {
        //     dd($response->json());
        // }
// 
        $certificatePath = public_path("/cacert.pem");
        $client = Http::withOptions([
            'verify' => false,
        ]);

        $response = $client->withHeaders([
            'Content-Type' => 'text/xml; charset="utf-8',
            'SOAPAction' => 'fleettestmw.cartrack.com/api/#get_vehiclelist',
        ])
        ->withBasicAuth('MALA00011', 'LIVERPOOL1//')
        ->withBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
                        <Body>
                            <endpoint.get_vehiclelist xmlns="fleettestmw.cartrack.com/api/">
                                <username>[string]</username>
                                <sub_username>[string]</sub_username>
                            </endpoint.get_vehiclelist>
                        </Body>
                    </Envelope>', 'text/xml')
        ->post('https://fleetmw.cartrack.com:443/api/index.php');

        $xml = $response->body();
        // // json
        // $response = json_encode(simplexml_load_string($xml));

        // $plainXML = $this->mungXML( trim($xml) );
        // $arrayResult = json_decode(json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML( $xml );
        $XMLresults     = $doc->getElementsByTagName("ns1_endpoint.get_vehiclelistResponse");
        $output = $XMLresults->item(0)->nodeValue;

        
                // $response = json_decode($response,TRUE);

        // if($response->ok()){
        //     return $response->json();
        // } else {
        //     dd($response->json());
        // }
        dd($output);

    }


    // FUNCTION TO MUNG THE XML SO WE DO NOT HAVE TO DEAL WITH NAMESPACE
    function mungXML($xml)
    {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key)
        {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
            = '#'               // REGEX DELIMITER
            . '('               // GROUP PATTERN 1
            . '\<'              // LOCATE A LEFT WICKET
            . '/?'              // MAYBE FOLLOWED BY A SLASH
            . preg_quote($key)  // THE NAMESPACE
            . ')'               // END GROUP PATTERN
            . '('               // GROUP PATTERN 2
            . ':{1}'            // A COLON (EXACTLY ONE)
            . ')'               // END GROUP PATTERN
            . '#'               // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
            = '$1'          // BACKREFERENCE TO GROUP 1
            . '_'           // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml =  preg_replace($rgx, $rep, $xml);
        }

        return $xml;

    } // End :: mungXML()

    public function test(Request $request){

        $username = "MALA00011";
        $key      = "c3b76a1be7da948b64514e1bf65084d2afe8d9dac69df4ca5a20f2fc41da8f65";
        $token    = base64_encode($username . ":" . $key);

        // dd($token);

        $response = Http::withHeaders([
            'Authorization' => 'Basic ' . $token
        ])->get('https://fleetapi-mw.cartrack.com/rest/vehicles'
        // , ['filter[date]' => '2022-07-01']
    );


   // $response = Http::withHeaders([
   //      'Authorization' => 'Token e6d1cb128a8dfcaef4deac0256c3cd6d035be1b9',
   //      'Account-Token' => 'ad6e42872e'
   //  ])->get('https://secure.fleetio.com/api/v1/vehicles');

   //  if ($response->successful()) {
   //      // handle successful response
   //      $data = $response->json();
   //  } else {
   //      // handle error response
   //      $errorMessage = $response->body();
   //  }

        if ($response->successful()) {
            // handle successful response
            $data = $response->json();
        } else {
            // handle error response
            $data = $response->body();
        }

        dd($data);
    }


    public function voiceToText(Request $request) {
        return view('assistants.voice-to-text');
    }

    public function imageOptimiser(Request $request) {

        $file = $request->image;
        
        if($file) {
            $fileName = $file->getClientOriginalName();

            $response = Http::timeout(600)->attach('files',$file->get() , $fileName)->post('http://api.resmush.it/?qlty=50');
            if ($response->successful()) {
                $result = $response->json();
            } else {
                $result = $response->serverError() ? 'Server Error' : 'Client Error';
            }     
        } else { $result = null; }

        return response($result);
    }



}
