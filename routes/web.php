<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\OpenAIController;
use App\Http\Controllers\PermissionController;


use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {

//     if( auth()->check() ) {
//         return redirect('/home');
//     }
//     return view('welcome');
// });

Auth::routes();

Route::get('/test', function(){


    dd("Auth Error");
});

Route::get('/', function(){

    // if (auth()->check()) {
    //     return redirect('/admin');
    // }
    return redirect('admin');
    return view('welcome');
});

Route::get('/pdf', function(){

    return view('pdf-worker');
});

Route::prefix('/')->middleware('auth')->group(function () {

    // Route::get('/', [HomeController::class, 'index'])->name('home');

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);

    Route::resource('openai', OpenAIController::class);
    Route::get('/openai-image-generators', [OpenAIController::class, 'imageGenerators']);
    Route::get('/gpt', [OpenAIController::class, 'gpt']);

    // Route::get('/image-optimizer', [OpenAIController::class, 'imageOptimizer']);
    Route::get('/voice-to-text', [OpenAIController::class, 'voiceToText']);


    Route::resource('users', UserController::class);
    Route::get('/ajax-users', [UserController::class, 'ajaxUsers']);
    Route::get('/profile', [UserController::class, 'profile']);
    Route::post('/profile/{user}', [UserController::class, 'profileSave']);
});



// Route::get('/test', [OpenAIController::class, 'test']);
// Route::get('/test2', [OpenAIController::class, 'test2']);
