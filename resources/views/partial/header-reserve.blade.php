
    <nav class="navbar navbar-top navbar-expand" id="navbarDefault" style="display:none;">
      <div class="collapse navbar-collapse justify-content-between">
        <div class="navbar-logo">
          <button class="btn navbar-toggler navbar-toggler-humburger-icon hover-bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
          <a class="navbar-brand me-1 me-sm-3" href="/">
            <div class="d-flex align-items-center">
              <div class="d-flex align-items-center"><img src="{{ asset('img/logo.PNG') }}" alt="phoenix" width="27" />
                <p class="logo-text ms-2 d-none d-sm-block">{{ env("APP_NAME")}}</p>
              </div>
            </div>
          </a>
        </div>
        <div class="search-box navbar-top-search-box d-none d-lg-block" data-list='{"valueNames":["title"]}' style="width:25rem;">
          <form class="position-relative" data-bs-toggle="search" data-bs-display="static"><input class="form-control search-input fuzzy-search rounded-pill form-control-sm" type="search" placeholder="Search..." aria-label="Search" />
            <span class="fas fa-search search-box-icon"></span>
          </form>
          <div class="btn-close position-absolute end-0 top-50 translate-middle cursor-pointer shadow-none" data-bs-dismiss="search"><button class="btn btn-link btn-close-falcon p-0" aria-label="Close"></button></div>
          <div class="dropdown-menu border border-300 font-base start-0 py-0 overflow-hidden w-100">
            <div class="scrollbar-overlay" style="max-height: 30rem;">
              <div class="list pb-3">
                <h6 class="dropdown-header text-1000 fs--2 py-2">24 <span class="text-500">results</span></h6>
                <hr class="text-200 my-0" />
                <h6 class="dropdown-header text-1000 fs--1 border-bottom border-200 py-2 lh-sm">People</h6>
                <div class="py-2">

                  @foreach( App\Models\User::paginate() as $user)
                  <a class="dropdown-item py-2 d-flex align-items-center" href="/chat/{{ $user->id }}">
                    <div class="avatar avatar-l status-online  me-2 text-900">
                      <img class="rounded-circle " src="{{ $user->image }}" alt="" />
                    </div>
                    <div class="flex-1">
                      <h6 class="mb-0 text-1000 title">{{ $user->name }}</h6>
                      <p class="fs--2 mb-0 d-flex text-700">{{ $user->email }}</p>
                    </div>
                  </a>
                  @endforeach

                </div>

              </div>
              <div class="text-center">
                <p class="fallback fw-bold fs-1 d-none">No Result Found.</p>
              </div>
            </div>
          </div>
        </div>

        @include('partial.header-user')
      </div>
    </nav>

    <nav class="navbar navbar-top navbar-slim navbar-expand" id="topNavSlim" style="display:none;">
      <div class="collapse navbar-collapse justify-content-between">
        <div class="navbar-logo">
          <button class="btn navbar-toggler navbar-toggler-humburger-icon hover-bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
          <a class="navbar-brand navbar-brand" href="/">{{ env("APP_NAME")}} <span class="text-1000 d-none d-sm-inline">slim</span></a>
        </div>
        @include('partial.header-user')
      </div>
    </nav>








    <nav class="navbar navbar-top navbar-slim justify-content-between navbar-expand-lg" id="navbarTopSlim" style="display:none;">
      <div class="navbar-logo">
        <button class="btn navbar-toggler navbar-toggler-humburger-icon hover-bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTopCollapse" aria-controls="navbarTopCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
        <a class="navbar-brand navbar-brand" href="/">{{ env("APP_NAME")}} 
          <span class="text-1000 d-none d-sm-inline">slim</span></a>
      </div>
      <div class="collapse navbar-collapse navbar-top-collapse order-1 order-lg-0 justify-content-center" id="navbarTopCollapse">
        <ul class="navbar-nav navbar-nav-top" data-dropdown-on-hovar="data-dropdown-on-hovar">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
              <span class="uil fs-0 me-2 uil-chart-pie"></span>Home
            </a>
            <ul class="dropdown-menu navbar-dropdown-caret">
              <li><a class="dropdown-item" href="/">
                  <div class="dropdown-item-wrapper"><span class="me-2 uil" data-feather="shopping-cart"></span>Home</div>
                </a></li>
              <li>>
            </ul>
          </li>
<!--           <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false"><span class="uil fs-0 me-2 uil-cube"></span>User Management</a>
            <ul class="dropdown-menu navbar-dropdown-caret">

              <li>
                <a class="dropdown-item" href="/users">
                  <div class="dropdown-item-wrapper">
                    <span class="me-2 uil" data-feather="message-square"></span>Users
                  </div>
                </a>
              </li>
    
            </ul>
          </li> -->

        </ul>
      </div>
      @include('partial.header-user')
    </nav>

    <nav class="navbar navbar-top navbar-expand-lg" id="navbarCombo" data-navbar-top="combo" data-move-target="#navbarVerticalNav" style="display:none;">
      <div class="navbar-logo">
        <button class="btn navbar-toggler navbar-toggler-humburger-icon hover-bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
        <a class="navbar-brand me-1 me-sm-3" href="../../index.html">
          <div class="d-flex align-items-center">
            <div class="d-flex align-items-center"><img src="../../img/logo.png" alt="phoenix" width="27" />
              <p class="logo-text ms-2 d-none d-sm-block">phoenix</p>
            </div>
          </div>
        </a>
      </div>
      <div class="collapse navbar-collapse navbar-top-collapse order-1 order-lg-0 justify-content-center" id="navbarTopCollapse">
        <ul class="navbar-nav navbar-nav-top" data-dropdown-on-hovar="data-dropdown-on-hovar">
          <li class="nav-item dropdown"><a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false"><span class="uil fs-0 me-2 uil-chart-pie"></span>Home</a>
            <ul class="dropdown-menu navbar-dropdown-caret">
              <li>
                <a class="dropdown-item" href="/">
                  <div class="dropdown-item-wrapper">
                    <span class="me-2 uil" data-feather="shopping-cart"></span>Home
                  </div>
                </a>
              </li>
            </ul>
          </li>

<!--           <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
              <span class="uil fs-0 me-2 uil-document-layout-right"></span>User Management
            </a>
            <ul class="dropdown-menu navbar-dropdown-caret">
              <li><a class="dropdown-item" href="/users">
                  <div class="dropdown-item-wrapper"><span class="me-2 uil" data-feather="monitor"></span>Users</div>
                </a>
              </li>
            </ul>
          </li> -->

        </ul>
      </div>
      @include('partial.header-user')
    </nav>

    <nav class="navbar navbar-top navbar-slim justify-content-between navbar-expand-lg" id="navbarComboSlim" data-navbar-top="combo" data-move-target="#navbarVerticalNav" style="display:none;">
      <div class="navbar-logo">
        <button class="btn navbar-toggler navbar-toggler-humburger-icon hover-bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation">
          <span class="navbar-toggle-icon">
              <span class="toggle-line"></span>
          </span>
        </button>
        <a class="navbar-brand navbar-brand" href="/">{{ env("APP_NAME")}} <span class="text-1000 d-none d-sm-inline">slim</span></a>
      </div>
      <div class="collapse navbar-collapse navbar-top-collapse order-1 order-lg-0 justify-content-center" id="navbarTopCollapse">
        <ul class="navbar-nav navbar-nav-top" data-dropdown-on-hovar="data-dropdown-on-hovar">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
              <span class="uil fs-0 me-2 uil-chart-pie"></span>Home
            </a>
            <ul class="dropdown-menu navbar-dropdown-caret">
              <li><a class="dropdown-item" href="/">
                  <div class="dropdown-item-wrapper"><span class="me-2 uil" data-feather="shopping-cart"></span>Home</div>
                </a>
              </li>
            </ul>
          </li>

        </ul>
      </div>  
      @include('partial.header-user')
    </nav>
