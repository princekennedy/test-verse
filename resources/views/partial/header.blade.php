
    <nav class="navbar navbar-vertical navbar-expand-lg" style="display:none;">
      <script>
        var navbarStyle = window.config.config.phoenixNavbarStyle;
        if (navbarStyle && navbarStyle !== 'transparent') {
          document.querySelector('body').classList.add(`navbar-${navbarStyle}`);
        }
      </script>
      <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
        <!-- scrollbar removed-->
        <div class="navbar-vertical-content">
          <ul class="navbar-nav flex-column" id="navbarVerticalNav">
            <li class="nav-item">
              <!-- parent pages-->
              <div class="nav-item-wrapper">
<!--                 <a class="nav-link dropdown-indicator label-1" href="#home" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="home">
                  <div class="d-flex align-items-center">
                    <div class="dropdown-indicator-icon"><span class="fas fa-caret-right"></span></div><span class="nav-link-icon"><span data-feather="pie-chart"></span></span><span class="nav-link-text">Home</span>
                  </div>
                </a> -->
                <div class="parent-wrapper label-1">
                  <ul class="nav collapse parent" data-bs-parent="#navbarVerticalCollapse" id="home">
                    <li class="collapsed-nav-item-title d-none">
                      <a href="/"> Home </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/" data-bs-toggle="" aria-expanded="false">
                        <div class="d-flex align-items-center">
                          <span class="nav-link-text">Report</span>
                        </div>
                      </a>
                    </li>

                  </ul>
                </div>
              </div>
            </li>

            @if (
                Auth::user()->can('view-any', App\Models\User::class) || 
                Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class)
              )
            <li class="nav-item">
              <!-- label-->
              <p class="navbar-vertical-label">Main Menu</p>
              <hr class="navbar-vertical-line" /><!-- parent pages-->
              @can('view-any', App\Models\User::class)
              <div class="nav-item-wrapper">
                <a class="nav-link label-1" href="/chat" role="button" data-bs-toggle="" aria-expanded="false">
                  <div class="d-flex align-items-center">
                    <span class="nav-link-icon">
                      <span data-feather="calendar"></span>
                    </span>
                    <span class="nav-link-text-wrapper">
                      <span class="nav-link-text">Chat</span>
                    </span>
                  </div>
                </a>
              </div> 
              @endcan    
            </li>
            @endif

            @if (
                Auth::user()->can('view-any', Spatie\Permission\Models\Role::class) || 
                Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class)
              )
            <li class="nav-item">
              <!-- label-->
              <p class="navbar-vertical-label">User Management</p>
              <hr class="navbar-vertical-line" /><!-- parent pages-->

              @can('view-any', App\Models\User::class)
              <div class="nav-item-wrapper">
                <a class="nav-link label-1" href="/users" role="button" data-bs-toggle="" aria-expanded="false">
                  <div class="d-flex align-items-center">
                    <span class="nav-link-icon">
                      <span data-feather="users"></span>
                    </span>
                    <span class="nav-link-text-wrapper">
                      <span class="nav-link-text">Users</span></span>
                    </div>
                </a>
              </div><!-- parent pages-->
              @endcan
              @can('view-any', Spatie\Permission\Models\Role::class )
              <div class="nav-item-wrapper">
                <a class="nav-link label-1" href="/roles" role="button" data-bs-toggle="" aria-expanded="false">
                  <div class="d-flex align-items-center">
                    <span class="nav-link-icon">
                      <span data-feather="users"></span>
                    </span>
                    <span class="nav-link-text-wrapper">
                      <span class="nav-link-text">Roles</span></span>
                    </div>
                </a>
              </div><!-- parent pages-->
              @endcan
     
            </li>

            @endcan
 

          </ul>
        </div>
      </div>
      <div class="navbar-vertical-footer"><button class="btn navbar-vertical-toggle border-0 fw-semi-bold w-100 white-space-nowrap d-flex align-items-center"><span class="uil uil-left-arrow-to-left fs-0"></span><span class="uil uil-arrow-from-right fs-0"></span><span class="navbar-vertical-footer-text ms-2">Collapsed View</span></button></div>
    </nav>

    <nav class="navbar navbar-top navbar-expand-lg" id="navbarTop" style="display:none;">
      <div class="navbar-logo">
        <button class="btn navbar-toggler navbar-toggler-humburger-icon hover-bg-transparent" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTopCollapse" aria-controls="navbarTopCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
        <a class="navbar-brand me-1 me-sm-3" href="/">
          <div class="d-flex align-items-center">
            <div class="d-flex align-items-center"><img src="{{ asset('img/logo.PNG') }}" alt="phoenix" width="27" />
              <p class="logo-text ms-2 d-none d-sm-block"> {{ env("APP_NAME")}} </p>
            </div>
          </div>
        </a>
      </div>
      <div class="collapse navbar-collapse navbar-top-collapse order-1 order-lg-0 justify-content-center" id="navbarTopCollapse">
        <ul class="navbar-nav navbar-nav-top" data-dropdown-on-hovar="data-dropdown-on-hovar">
         
          <li class="nav-item dropdown">
            <a class="nav-link  lh-1" href="/" role="button">
              <span class="uil fs-0 me-2 uil-chart-pie"></span>Home
            </a>

            <!-- dropdown-toggle  data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false"-->
            <ul class="dropdown-menu navbar-dropdown-caret">
              <li>
                <a class="dropdown-item" href="/">
                  <div class="dropdown-item-wrapper">
                    <span class="me-2 uil" data-feather="shopping-cart"></span>
                    Reports
                  </div>
                </a>
              </li>
            </ul>
          </li>

          @if (
            Auth::user()->can('view-any', App\Models\User::class) || 
            Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class)
          )
          <li class="nav-item dropdown"><a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false"><span class="uil fs-0 me-2 uil-cube"></span>Main Menu</a>
            <ul class="dropdown-menu navbar-dropdown-caret">
              @can('view-any', App\Models\User::class)
              <li>
                <a class="dropdown-item" href="/chat">
                  <div class="dropdown-item-wrapper">
                    <span class="me-2 uil" data-feather="mail"></span>
                    Chat
                  </div>
                </a>
              </li>   
              @endcan
            </ul>
          </li>
          @endif

            @if (
              Auth::user()->can('view-any', Spatie\Permission\Models\Role::class) || 
              Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class)
            )
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
              <span class="uil fs-0 me-2 uil-files-landscapes-alt"></span>User Management
            </a>
            <ul class="dropdown-menu navbar-dropdown-caret">
              @can('view-any', App\Models\User::class)
              <li>
                <a class="dropdown-item" href="/users">
                  <div class="dropdown-item-wrapper">
                    <span class="me-2 uil" data-feather="users"></span>Users
                  </div>
                </a>
              </li>
              @endcan

              @can('view-any', Spatie\Permission\Models\Role::class)
              <li>
                <a class="dropdown-item" href="/roles">
                  <div class="dropdown-item-wrapper">
                    <span class="me-2 uil" data-feather="users"></span>Roles
                  </div>
                </a>
              </li>
              @endcan

            </ul>
          </li>
          @endif

          @if (
            Auth::user()->can('view-any', App\Models\User::class) || 
            Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class)
          )
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle lh-1" href="#!" role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
              <span class="uil fs-0 me-2 uil-document-layout-right"></span>Configurations
            </a>

            <ul class="dropdown-menu navbar-dropdown-caret">
              
             </ul>
          </li>
          @endif

        </ul>
      </div>

      @include('partial.header-user')

    </nav>


    @include('partial.header-reserve')
    @include('modals.search-modal')

    @push('scripts')

    <script type="text/javascript">

      if( window.config ) {
        var navbarTopShape = window.config.config.phoenixNavbarTopShape;
        var navbarPosition = window.config.config.phoenixNavbarPosition;
      } else {
        var navbarTopShape = "default";
        var navbarPosition = "horizontal";
      }        

      var body = document.querySelector('body');
      var navbarDefault = document.querySelector('#navbarDefault');
      var navbarTop = document.querySelector('#navbarTop');
      var topNavSlim = document.querySelector('#topNavSlim');
      var navbarTopSlim = document.querySelector('#navbarTopSlim');
      var navbarCombo = document.querySelector('#navbarCombo');
      var navbarComboSlim = document.querySelector('#navbarComboSlim');


      var documentElement = document.documentElement;
      var navbarVertical = document.querySelector('.navbar-vertical');

      if (navbarTopShape === 'slim' && navbarPosition === 'vertical') {
        navbarDefault.remove();
        navbarTop.remove();
        navbarTopSlim.remove();
        navbarCombo.remove();
        navbarComboSlim.remove();
        topNavSlim.style.display = 'block';
        navbarVertical.style.display = 'inline-block';
        body.classList.add('nav-slim');
      } else if (navbarTopShape === 'slim' && navbarPosition === 'horizontal') {
        navbarDefault.remove();
        navbarVertical.remove();
        navbarTop.remove();
        topNavSlim.remove();
        navbarCombo.remove();
        navbarComboSlim.remove();
        navbarTopSlim.removeAttribute('style');
        body.classList.add('nav-slim');
      } else if (navbarTopShape === 'slim' && navbarPosition === 'combo') {
        navbarDefault.remove();
        //- navbarVertical.remove();
        navbarTop.remove();
        topNavSlim.remove();
        navbarCombo.remove();
        navbarTopSlim.remove();
        navbarComboSlim.removeAttribute('style');
        navbarVertical.removeAttribute('style');
        body.classList.add('nav-slim');
      } else if (navbarTopShape === 'default' && navbarPosition === 'horizontal') {
        navbarDefault.remove();
        topNavSlim.remove();
        navbarVertical.remove();
        navbarTopSlim.remove();
        navbarCombo.remove();
        navbarComboSlim.remove();
        navbarTop.removeAttribute('style');
        documentElement.classList.add('navbar-horizontal');
      } else if (navbarTopShape === 'default' && navbarPosition === 'combo') {
        topNavSlim.remove();
        navbarTop.remove();
        navbarTopSlim.remove();
        navbarDefault.remove();
        navbarComboSlim.remove();
        navbarCombo.removeAttribute('style');
        navbarVertical.removeAttribute('style');
        documentElement.classList.add('navbar-combo')

      } else {

        navbarDefault.remove();
        topNavSlim.remove();
        navbarVertical.remove();
        navbarTopSlim.remove();
        navbarCombo.remove();
        navbarComboSlim.remove();
        navbarTop.removeAttribute('style');
        documentElement.classList.add('navbar-horizontal');

        // topNavSlim.remove();
        // navbarTop.remove();
        // navbarTopSlim.remove();
        // navbarCombo.remove();
        // navbarComboSlim.remove();
        // navbarDefault.removeAttribute('style');
        // navbarVertical.removeAttribute('style');
      }

      var navbarTopStyle = window.config.config.phoenixNavbarTopStyle;
      var navbarTop = document.querySelector('.navbar-top');
      if (navbarTopStyle === 'darker') {
        navbarTop.classList.add('navbar-darker');
      }

      var navbarVerticalStyle = window.config.config.phoenixNavbarVerticalStyle;
      var navbarVertical = document.querySelector('.navbar-vertical');
      if (navbarVerticalStyle === 'darker') {
        navbarVertical.classList.add('navbar-darker');
      }

    </script>

    @endpush