<div class="">

	@if( session('success') )
    <div class="alert alert-outline-success d-flex align-items-center" role="alert">
      <span class="fas fa-check-circle text-success fs-3 me-3"></span>
      <p class="mb-0 flex-1">{{ session('success') }}</p>
      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if( session('error') )
    <div class="alert alert-outline-danger d-flex align-items-center" role="alert">
      <span class="fas fa-times-circle text-danger fs-3 me-3"></span>
      <p class="mb-0 flex-1">{{  session('error') }}</p>
      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-outline-danger d-flex align-items-center" role="alert">
      <span class="fas fa-times-circle text-danger fs-3 me-3"></span>
      <p class="mb-0 flex-1">{{  $error }}</p>
      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endforeach
    @endif

    
</div>