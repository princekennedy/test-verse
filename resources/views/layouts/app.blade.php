<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">

        <meta name="application-name" content="{{ config('app.name') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script> -->
        
        <script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>
        <link rel="stylesheet" href="https://unpkg.com/flowbite@1.5.1/dist/flowbite.min.css" />

        <title>{{ config('app.name') }}</title>

        <style>[x-cloak] { display: none !important; }</style>
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        @livewireStyles
        @stack('styles')
        @livewireScripts
        @stack('scripts')
    </head>

    <body class="antialiased">
        {{ $slot ?? '' }}

        @livewire('notifications')

        <script src="https://unpkg.com/flowbite@1.5.1/dist/flowbite.js"></script>

    </body>
</html>
