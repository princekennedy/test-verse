<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr" class="navbar-horizontal chrome windows fontawesome-i2svg-active fontawesome-i2svg-complete">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>{{ env("APP_NAME") }} </title>
        
        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>

        <script type="text/javascript" src="{{ asset('openai-assets/artyom.window.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/notyf@3/notyf.min.css">
        
        <!-- Icons -->
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        
        <script src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js" defer></script>
        
        <script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>

        <!-- ===============================================-->
        <!--    Favicons-->
        <!-- ===============================================-->
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/logo.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/logo.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/logo.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo.png') }}">
        <!-- <link rel="manifest" href=" asset('assets/img/favicons/manifest.json') }}"> -->
        <meta name="msapplication-TileImage" content="{{ asset('img/logo.png') }}">
        <meta name="theme-color" content="#ffffff">
        <script src="{{ asset('vendors/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ asset('vendors/simplebar/simplebar.min.js') }}"></script>
        <script src="{{ asset('assets/js/config.js') }}"></script>


        <!-- PWA  -->
        <meta name="theme-color" content="#6777ef"/>
        <link rel="apple-touch-icon" href="{{ asset('img/logo.PNG') }}">
        <link rel="manifest" href="{{ asset('/manifest.json') }}">

        <!-- ===============================================-->
        <!--    Stylesheets-->
        <!-- ===============================================-->
        <link href="{{ asset('vendors/choices/choices.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/flatpickr/flatpickr.min.css') }}" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com/">
        <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin="">
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&amp;display=swap" rel="stylesheet">
        <link href="{{ asset('vendors/simplebar/simplebar.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('vendors/glightbox/glightbox.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/line.css') }}">
        <link href="{{ asset('assets/css/theme-rtl.min.css') }}" type="text/css" rel="stylesheet" id="style-rtl">
        <link href="{{ asset('assets/css/theme.min.css') }}" type="text/css" rel="stylesheet" id="style-default">
        <link href="{{ asset('assets/css/user-rtl.min.css') }}" type="text/css" rel="stylesheet" id="user-style-rtl">
        <link href="{{ asset('assets/css/user.min.css') }}" type="text/css" rel="stylesheet" id="user-style-default">
        <script>
          var phoenixIsRTL = window.config.config.phoenixIsRTL;
          if (phoenixIsRTL) {
            var linkDefault = document.getElementById('style-default');
            var userLinkDefault = document.getElementById('user-style-default');
            linkDefault.setAttribute('disabled', true);
            userLinkDefault.setAttribute('disabled', true);
            document.querySelector('html').setAttribute('dir', 'rtl');
          } else {
            var linkRTL = document.getElementById('style-rtl');
            var userLinkRTL = document.getElementById('user-style-rtl');
            linkRTL.setAttribute('disabled', true);
            userLinkRTL.setAttribute('disabled', true);
          }
        </script>
        @stack('styles')

        
        <script type="module">
            // import hotwiredTurbo from 'https://cdn.skypack.dev/@hotwired/turbo';
        </script>

        @livewireStyles

        <!-- laravelPWA -->
    </head>
    
    <body>

        <!-- ===============================================-->
        <!--    Main Content-->
        <!-- ===============================================-->
        <main class="main" id="top">
            <div class="container-fluid px-0" data-layout="container">
                
                @include('partial.header')
                
                <div class="content">
                    
                    @include('partial.messages')

                    @yield('content')
                    
                    @include('partial.footer')

                </div>
            </div>
        </main>

        @include('partial.settings')

        @stack('modals')
        
        @livewireScripts
        

        <script src="{{ asset('vendors/popper/popper.min.js') }}"></script>
        <script src="{{ asset('vendors/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vendors/anchorjs/anchor.min.js') }}"></script>
        <script src="{{ asset('vendors/is/is.min.js') }}"></script>
        <script src="{{ asset('vendors/fontawesome/all.min.js') }}"></script>
        <script src="{{ asset('vendors/lodash/lodash.min.js') }}"></script>
        <script src="{{ asset('js/polyfill.min58be.js?features=window.scroll') }}"></script>
        <script src="{{ asset('vendors/list.js/list.min.js') }}"></script>
        <script src="{{ asset('vendors/feather-icons/feather.min.js') }}"></script>
        <script src="{{ asset('vendors/dayjs/dayjs.min.js') }}"></script>
        <script src="{{ asset('vendors/glightbox/glightbox.min.js') }}"></script>
        <script src="{{ asset('vendors/choices/choices.min.js') }}"></script>
        <script src="{{ asset('vendors/lottie/lottie.min.js') }}"></script>
        <script src="{{ asset('assets/js/phoenix.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>
        
            
        <script src="https://cdn.jsdelivr.net/npm/notyf@3/notyf.min.js"></script>
        <script type="text/javascript">
            var notyf = new Notyf({dismissible: true})
        </script>
        @stack('scripts')
        
        @if (session()->has('success')) 
        <script>
            notyf.success('{{ session('success') }}')
        </script> 
        @endif   

        @if (session()->has('error')) 
        <script>
            notyf.error('{{ session('error') }}')
        </script> 
        @endif
        
        <script>
            /* Simple Alpine Image Viewer */
            document.addEventListener('alpine:init', () => {
                Alpine.data('imageViewer', (src = '') => {
                    return {
                        imageUrl: src,
        
                        refreshUrl() {
                            this.imageUrl = this.$el.getAttribute("image-url")
                        },
        
                        fileChosen(event) {
                            this.fileToDataUrl(event, src => this.imageUrl = src)
                        },
        
                        fileToDataUrl(event, callback) {
                            if (! event.target.files.length) return
        
                            let file = event.target.files[0],
                                reader = new FileReader()
        
                            reader.readAsDataURL(file)
                            reader.onload = e => callback(e.target.result)
                        },
                    }
                })
            })
        </script>

        <script src="{{ asset('/sw.js') }}"></script>
        <script>
            if (!navigator.serviceWorker.controller) {
                navigator.serviceWorker.register("/sw.js").then(function (reg) {
                    console.log("Service worker has been registered for scope: " + reg.scope);
                });
            }


            let deferredPrompt;
            window.addEventListener('beforeinstallprompt', (e) => {
                deferredPrompt = e;
            });

            const installApp = document.getElementById('installApp');
            if( installApp ) {
                installApp.addEventListener('click', async () => {
                    if (deferredPrompt !== null) {
                        deferredPrompt.prompt();
                        const { outcome } = await deferredPrompt.userChoice;
                        if (outcome === 'accepted') {
                            deferredPrompt = null;
                        }
                    }
                });                
            }


        </script>
    </body>
</html>