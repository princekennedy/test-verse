@extends('layouts.layout')

@section('content')

<div class="mb-9" id="home-el">
  
  <div class="row g-4">

    <div class="col-sm-6 col-md-4 col-lg-3">
      <a href="/chat" class="card border border-primary">
        <div class="card-body">
          <h4 class="card-title">Chat</h4>
          <p class="card-text">Simple description.</p>
        </div>
      </a>
    </div>

    <div class="col-sm-6 col-md-4 col-lg-3">
      <a href="/openai" class="card border border-primary">
        <div class="card-body">
          <h4 class="card-title">Machine</h4>
          <p class="card-text">Simple description.</p>
        </div>
      </a>
    </div>

    <div class="col-sm-6 col-md-4 col-lg-3">
      <a href="/voice-to-text" class="card border border-primary">
        <div class="card-body">
          <h4 class="card-title">Voice to Text</h4>
          <p class="card-text">Make sure you have enabled microphone.</p>
        </div>
      </a>
    </div>

    <div class="col-sm-6 col-md-4 col-lg-3">
      <a href="/admin/image-optimizer" class="card border border-primary">
        <div class="card-body">
          <h4 class="card-title">Image Optimizer</h4>
          <p class="card-text">Optimize Images without losing quality.</p>
        </div>
      </a>
    </div>

    @if( auth()->user()->isSuperAdmin())
    <div class="col-sm-6 col-md-4 col-lg-3">
      <a href="https://chat.openai.com/chat" class="card border border-primary">
        <div class="card-body">
          <h4 class="card-title">GPT</h4>
          <p class="card-text">Simple description.</p>
        </div>
      </a>
    </div>
    @endif

  </div>

</div>

@endsection



@push('scripts')

    <script type="text/javascript">
        new Vue({

            el: "#home-el",

            data(){
                return {
                    application: null,
                    isLoading: false,
                }
            },

            methods: {
                getApplication(application_id){
                    axios.get("/ajax-application/" + application_id).then( res => {
                      this.application = res.data;
                      console.log( this.application);
                    });
                },
            },

            created(){
            }

        })
    </script>

@endpush