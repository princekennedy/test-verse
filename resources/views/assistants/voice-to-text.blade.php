@extends('layouts.layout')

@push('styles')
    <style type="text/css">

        body{
/*            overflow: hidden;*/
        }

        .content {
            padding: 0px;
            margin: 20px auto ;
        }

        .footer {
            display: none;
        }

        .messages {
            overflow: hidden auto;
        }


        /* Styles for screens smaller than 320px */
        @media only screen and (max-width: 320px) {
          /* Your CSS styles here */
        }

        /* Styles for screens between 321px and 480px */
        @media only screen and (min-width: 321px) and (max-width: 480px) {
          /* Your CSS styles here */
            .messages {
                width: 100%; 
                height: 70vh; 
            }            

            .message-footer {
                width: 100%; 
                height:30vh; 
                overflow: hidden; 
                width: 100%;
            }
        }

        /* Styles for screens between 481px and 768px */
        @media only screen and (min-width: 481px) and (max-width: 768px) {
          /* Your CSS styles here */
            .messages {
                width: 100%; 
                height: 70vh; 
            }            

            .message-footer {
                width: 100%; 
                height:30vh; 
                overflow: hidden; 
                width: 100%;
            }

        }

        /* Styles for screens between 769px and 1024px */
        @media only screen and (min-width: 769px) and (max-width: 1024px) {
          /* Your CSS styles here */
            .messages {
                width: 100%; 
                height: 75vh; 
            }            

            .message-footer {
                width: 100%; 
                height:25vh; 
                overflow: hidden; 
                width: 100%;
            }
        }

        /* Styles for screens larger than 1024px */
        @media only screen and (min-width: 1025px) {
            /* Your CSS styles here */

            .messages {
                width: 100%; 
                height: 80vh; 
            }            

            .message-footer {
                width: 100%; 
                height:20vh; 
                overflow: hidden; 
                width: 100%;
            }

        }

    </style>
@endpush

@section('content')

    <div id="chat-el" class="p-2">

        <div class=" chat-cover " style="width: 100%; height:95vh; overflow: hidden; margin-top: 5px;">


            <section class="px-lg-1 messages" id="comment-parent">

                <div class="list-group hover-actions-trigger py-3">

                    <a  v-for="(message, index) in messages" class="list-group-item list-group-item-action flex-column align-items-start p-3 p-sm-4" href="#!">
                      <div class="d-flex flex-column flex-sm-row justify-content-between mb-1 mb-md-0">
                        <!-- <h5 class="mb-1">What is ordered list?</h5> -->
                        <small class="text-600" v-text="message.dateTime"></small>
                      </div>
                      <p class="mb-1" v-html="message.text"></p>
                      <small class="text-600"></small>
                    </a>
                
                </div>


            </section>

            <section class="message-footer">
                <input type="text" v-model="search" class="form-control mb-2" placeholder="Type message"/>
                <!-- <textarea v-model="search" class="form-control mb-2" placeholder="Type message"></textarea> -->
                <div class="d-flex justify-content-between align-items-end mb-3">
                    <div class="d-flex justify-content-between align-items-end">
     <!--                    <input class="d-none" type="file" id="chatAttachment-0" />
                        <button class="btn btn-light btn-sm">
                            <span class="fa-solid fa-microphone"></span>
                        </button>
                        <button class="btn btn-light btn-sm">
                            <span class="fa-solid fa-ellipsis"></span>
                        </button> -->
                        <div class="">
                            <select v-model="command" class="form-control bg-light">
                                <option selected value="search">Assistance</option>
                                <option value="fix-spelling">Fix Spellings</option>
                                <option value="image-generations">Image Generation</option>
                                <option value="bible-question">Bible Question</option>
                                <option value="moderations">Judgement</option>
                            </select>
                        </div>
                
                    </div>
                    <div>
                        <button :disabled="search == '' ? true : isLoading" class="btn btn-primary fs--2" @click="loadingInput" type="button">
                            <span v-if="!isLoading">Send</span>
                            <span v-if="isLoading">Loading... </span>
                            <span class="fa-solid fa-paper-plane ms-1"></span>
                        </button>
                    </div>
                </div>
            </section>
            
        </div>

    </div>




@endsection


@push("scripts")
    <script type="text/javascript">
        new Vue({
            el: "#chat-el",
            data(){
                return {
                    search: null,
                    text: null,
                    result: null,
                    messages: [],
                    isLoading: false,
                    search: '',
                    text: '',
                    result: null,
                    command: 'search',
                    Jarvis: null,
                    error: '',
                }
            },

            methods: {

                getResult(){
                    axios.get("/openai/" + this.command + "?search=" + this.search).then( res => {
                        if( res.data.error == null){
                            this.text = res.data.text;
                            this.result = res.data.result;
                            this.error = '';
                            console.log(res.data)
                        } else { this.error = res.data.error; }
                        
                        // this.read();
                        this.messages.push({
                            name: "A",
                            text: this.text + this.error,
                            result: this.result,
                            dateTime: moment().format("LL"),
                            me: false,
                        })
                        this.isLoading = false;
                        this.handleScroll('#comment-parent');
                    });
                },

                read(){
                    /**
                     * To speech text
                     */
                    var text = this.search;
                    text = text.replaceAll("<br>", " ");

                    if( this.Jarvis == '' ) {
                        console.log( this.Jarvis );
                        return;
                    }

                    if( text == null || text == '') {
                        this.Jarvis.say("Sorry no text result found!");
                        return;
                    }

                    this.Jarvis.say( text, {
                        onStart: () => {
                            console.log("Reading ...");
                        },
                        onEnd: () => {
                            console.log("No more text to talk");
                        }
                    });
                },

                appendingQuestion(){
                    this.messages.push({
                        name: "Q",
                        text: this.search,
                        result: this.result,
                        dateTime: moment().format("LL"),
                        me: true,
                    });
                    this.handleScroll('#comment-parent');
                },

                remove(index) {
                    this.messages.splice(index,1);
                },

                loadingInput(){

                    if(this.search == null || this.search == '') return;
                    this.isLoading = true;
                    this.result = null;
                    this.appendingQuestion()
                    this.getResult()
                    this.search = '';
                    // setTimeout(() => {
                        // this.isLoading = false;
                        // this.appendingAnswer();
                    // }, 4000);
                },

                handleScroll(id){
                  $(id).animate({ scrollTop: $(id).prop("scrollHeight")}, 1000);
                },

                voiceCommands() {
                    var artyom = new Artyom();
                    var UserDictation = artyom.newDictation({
                        continuous:true, // Enable continuous if HTTPS connection
                        onResult:(text) => {
                            // Do something with the text
                            if(text === '' || text === null) return;
                            text = text.trim();
                            console.log(text);
                            if(text == 'done' || text == 'send' || text == 'period') {
                                console.log(90)
                                this.loadingInput();
                                return;
                            } 
                            else if(text == 'clear') {
                                this.search = '';
                            } 
                            else {
                                this.search += " " + text;
                            }

                        },
                        onStart:function(){
                            console.log("Dictation started by the user");
                        },
                        onEnd:function(){
                            alert("Dictation stopped by the user");
                        }
                    });

                    UserDictation.start();

                    // Stop whenever you want
                    // UserDictation.stop();
                },

            },

            mounted(){
                this.handleScroll('#comment-parent');
                this.Jarvis = new Artyom();

            },

            created(){
                this.Jarvis = new Artyom();
                this.voiceCommands()
            }
        });


        // var artyom = new Artyom();

        // // Start the commands !
        // artyom.initialize({
        //     lang: "en-GB", // GreatBritain english
        //     continuous: true, // Listen forever
        //     soundex: true,// Use the soundex algorithm to increase accuracy
        //     debug: true, // Show messages in the console
        //     executionKeyword: "and do it now",
        //     listen: true, // Start to listen commands !

        //     // If providen, you can only trigger a command if you say its name
        //     // e.g to trigger Good Morning, you need to say "Jarvis Good Morning"
        //     name: "Jarvis" 
        // }).then(() => {
        //     console.log("Artyom has been succesfully initialized");
        // }).catch((err) => {
        //     console.error("Artyom couldn't be initialized: ", err);
        // });


        // var artyom = new Artyom();
        // var UserDictation = artyom.newDictation({
        //     continuous:true, // Enable continuous if HTTPS connection
        //     onResult:function(text){
        //         // Do something with the text
        //         console.log(text);
        //     },
        //     onStart:function(){
        //         console.log("Dictation started by the user");
        //     },
        //     onEnd:function(){
        //         alert("Dictation stopped by the user");
        //     }
        // });

        // UserDictation.start();

        // // Stop whenever you want
        // // UserDictation.stop();


    </script>
@endpush