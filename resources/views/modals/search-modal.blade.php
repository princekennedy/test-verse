<div class="modal fade" id="searchBoxModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="true" data-phoenix-modal="data-phoenix-modal" style="--phoenix-backdrop-opacity: 1;">
  <div class="modal-dialog">
    <div class="modal-content mt-15 rounded-pill">
      <div class="modal-body p-0">
        <div class="search-box navbar-top-search-box" id="search-el" data-list='{"valueNames":["title"]}' style="width: auto;">
          <form class="position-relative" data-bs-toggle="search" data-bs-display="static">
            <input v-model="search" @input="getUsers" class="form-control search-input uzzy-search rounded-pill form-control-lg" type="search" placeholder="Search..." aria-label="Search" />
            <span class="fas fa-search search-box-icon"></span>
          </form>
          <div class="btn-close position-absolute end-0 top-50 translate-middle cursor-pointer shadow-none" data-bs-dismiss="search">
            <button class="btn btn-link btn-close-falcon p-0" aria-label="Close"></button>
          </div>
          <div class="dropdown-menu border border-300 font-base start-0 py-0 overflow-hidden w-100">
            <div class="scrollbar-overlay" style="max-height: 30rem;">
              <div class="list pb-3">

                <h6 class="dropdown-header text-1000 fs--2 py-2">
                  <span v-text="users.data.length"></span> 
                  <span class="text-500">results</span></h6>
                <hr class="text-200 my-0" />
                <h6 class="dropdown-header text-1000 fs--1 border-bottom border-200 py-2 lh-sm">People </h6>
                <div class="py-2">

                  <a class="dropdown-item" v-for="(user, index) in users.data" :href="'/chat/' + user.id">
                    <div class="d-flex align-items-center">
                      <div class="fw-normal text-1000 title"> 
                        <div class="avatar avatar-l ">
                          <img class="rounded-circle " :src="user.image" alt="" />
                        </div>
                        <span v-text="user.name"></span>
                      </div>
                    </div>
                  </a>

                </div>

              </div>
              
              <div class="text-center">
                <p class="fallback fw-bold fs-1 d-none">No Result Found.</p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

@push('scripts')

  <script type="text/javascript">
    new Vue({
      el: "#search-el",

      data(){
        return {
          search: "",
          users: {
            data: [],
          }
        }
      },

      methods: {

        getUsers(){
          axios.get("/ajax-users?search=" + this.search).then( res => {
            this.users = res.data;
            if( this.users.data.length == 0) {
              $(".fallback").removeClass("d-none");
            } else{
              $(".fallback").addClass("d-none");
            }
          });
        }


      },

      created(){
        this.getUsers();
      }

    })
  </script>

@endpush