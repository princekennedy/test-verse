@extends('layouts.guest')

@section('content')

      <div class="bg-white sticky-top landing-navbar" data-navbar-shadow-on-scroll="data-navbar-shadow-on-scroll">
        <nav class="navbar navbar-expand-lg container-small px-3 px-lg-7 px-xxl-3"><a class="navbar-brand flex-1 flex-lg-grow-0" href="">
            <div class="d-flex align-items-center"><img src="{{ asset('assets/img/icons/logo.png') }}" alt="phoenix" width="27" />
              <p class="logo-text ms-2"> {{ env('APP_NAME') }} </p>
            </div>
          </a>
          <div class="d-lg-none">
            <div class="theme-control-toggle fa-icon-wait px-2"><input class="form-check-input ms-0 theme-control-toggle-input" type="checkbox" data-theme-control="phoenixTheme" value="dark" id="themeControlToggleSm" /><label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggleSm" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch theme"><span class="icon" data-feather="moon"></span></label><label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggleSm" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch theme"><span class="icon" data-feather="sun"></span></label></div>
          </div><button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item border-bottom border-bottom-lg-0"><a class="nav-link lh-1 py-0 fs--1 fw-bold py-3 active" aria-current="page" href="#">Home</a></li>
              <li class="nav-item border-bottom border-bottom-lg-0"><a class="nav-link lh-1 py-0 fs--1 fw-bold py-3" href="#feature">Features</a></li>
              <li class="nav-item border-bottom border-bottom-lg-0"><a class="nav-link lh-1 py-0 fs--1 fw-bold py-3" href="#blog">Blog</a></li>
              <li class="nav-item"><a class="nav-link lh-1 py-0 fs--1 fw-bold py-3" href="#team">Team</a></li>
            </ul>
            <div class="d-grid d-lg-flex align-items-center">
              <div class="nav-item d-flex align-items-center d-none d-lg-block pe-2">
                <div class="theme-control-toggle fa-icon-wait px-2">
                  <input class="form-check-input ms-0 theme-control-toggle-input" type="checkbox" data-theme-control="phoenixTheme" value="dark" id="themeControlToggle" />
                  <label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch theme">
                    <span class="icon" data-feather="moon"></span>
                  </label>
                  <label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch theme">
                    <span class="icon" data-feather="sun"></span>
                  </label></div>
              </div>
                <a class="btn btn-link text-900 order-1 order-lg-0 ps-3 me-2" href="/login">Sign in</a>
                <a class="btn btn-phoenix-primary order-0" href="/register">
                  <span class="fw-bold">Sign Up</span>
                </a>
            </div>
          </div>
        </nav>
      </div>
      <div class="modal fade" id="searchBoxModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="true" data-phoenix-modal="data-phoenix-modal" style="--phoenix-backdrop-opacity: 1;">
        <div class="modal-dialog">
          <div class="modal-content mt-15">
            <div class="modal-body p-0">
              <div class="chat-search-box">
                <div class="form-icon-container"><input class="form-control py-3 form-icon-input rounded-1" type="text" autofocus="autofocus" placeholder="Search People, Groups and Messages" /><span class="fa-solid fa-magnifying-glass fs--1 form-icon"></span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section class="pb-8 overflow-hidden" id="home">
        <div class="hero-header-container-alternate position-relative">
          <div class="container-small px-lg-7 px-xxl-3">
            <div class="row align-items-center">
              <div class="col-12 col-lg-6 pt-8 pb-6 position-relative z-index-5 text-center text-lg-start">
                <h1 class="fs-5 fs-md-6 fs-xl-7 fw-black mb-4">
                  <span class="text-gradient-info me-3"> {{ env('APP_NAME')}} </span> Application <br />
                  <a class="btn btn-lg btn-primary rounded-pill me-3" href="/login" role="button">Sign In</a>
                  <a class="btn btn-link me-2 fs-0 p-0 text-decoration-none" href="/register" role="button">Sign Up
                    <span class="fa-solid fa-angle-right ms-2 fs--1"></span>
                  </a>
              </div>
              <div class="col-12 col-lg-auto d-none d-lg-block">
                <div class="hero-image-container position-absolute h-100 end-0 d-flex align-items-center">
                  <div class="position-relative">
                    <div class="position-absolute end-0 hero-image-container-overlay" style="transform: skewY(-8deg)."></div><img class="position-absolute end-0 hero-image-container-bg" src="../../assets/img/bg/bg-36.png" alt="" /><img class="w-100 d-dark-none rounded-2 hero-image-shadow" src="../../assets/img/bg/bg-34.png" alt="hero-header" /><img class="w-100 d-light-none rounded-2 hero-image-shadow" src="../../assets/img/bg/bg-35.png" alt="hero-header" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container-small px-md-8 mb-8 d-lg-none">
            <div class="position-relative">
              <div class="position-absolute end-0 hero-image-container-overlay"></div><img class="position-absolute top-50 hero-image-container-bg" src="../../assets/img/bg/bg-39.png" alt="" /><img class="img-fluid ms-auto d-dark-none rounded-2 hero-image-shadow" src="../../assets/img/bg/bg-34.png" alt="hero-header" /><img class="img-fluid ms-auto d-light-none rounded-2 hero-image-shadow" src="../../assets/img/bg/bg-35.png" alt="hero-header" />
            </div>
          </div>
        </div>
      </section>



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="pt-13 pb-10" id="feature">
        <div class="container-small px-lg-7 px-xxl-3">
          <div class="text-center mb-10 mb-md-5">
            <h5 class="text-info mb-3">Features</h5>
            <h2 class="mb-3 lh-base">A fully integrated suite of <br />payments products</h2>
            <p class="mb-0">Focus only on functionalities for your digital products with {{ env('APP_NAME')}}! Leave the UIs for us.</p>
            <div class="text-center mt-5"><a class="btn btn-outline-primary" href="#!">See more<span class="fa-solid fa-angle-right ms-2"></span></a></div>
          </div>
          <div class="row flex-between-center px-xl-11 mb-10 mb-md-9">
            <div class="col-md-6 order-1 order-md-0 text-center text-md-start">
              <h4 class="mb-3">Recieve the signals instantly</h4>
              <p class="mb-5">{{ env('APP_NAME')}} allows you to receive every signal instantly and fruitfully. No need for long waits.</p><a class="btn btn-link me-2 p-0 fs--1" href="#!" role="button">Check Demo<i class="fa-solid fa-angle-right ms-2"></i></a>
            </div>
            <div class="col-md-5 mb-5 mb-md-0 text-center"><img class="w-75 w-md-100 d-dark-none" src="../../assets/img/spot-illustrations/34.png" alt="" /><img class="w-75 w-md-100 d-light-none" src="../../assets/img/spot-illustrations/34_2.png" alt="" /></div>
          </div>

        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="pb-14 overflow-x-hidden">
        <div class="container-small px-lg-7 px-xxl-3">
          <div class="text-center mb-5 position-relative">
            <h5 class="text-info mb-3">Testimonial</h5>
            <h2 class="mb-2 lh-base">What our customers has to say about us</h2>
          </div>
          <div class="carousel testimonial-carousel slide position-relative dark__bg-1100" id="carouselExampleIndicators" data-bs-ride="carousel">
            <div class="bg-holder d-none d-md-block" style="background-image:url(../../assets/img/bg/39.png);background-size:186px;background-position:top 20px right 20px;"></div>
            <!--/.bg-holder-->
            <img class="position-absolute d-none d-lg-block" src="../../assets/img/bg/bg-left-22.png" width="150" alt="" style="top: -100px; left: -70px" /><img class="position-absolute d-none d-lg-block" src="../../assets/img/bg/bg-right-22.png" width="150" alt="" style="bottom: -80px; right: -80px" />
            <div class="carousel-inner">
              <div class="carousel-item text-center py-8 px-5 px-xl-15 active"><span class="fa fa-star text-warning"></span><span class="fa fa-star text-warning"></span><span class="fa fa-star text-warning"></span><span class="fa fa-star text-warning"></span><span class="fa fa-star text-warning"></span>
                <h3 class="fw-semi-bold fst-italic mt-3 mb-8 w-xl-70 mx-auto lh-base">Amazing app, excellent support from {{ env('APP_NAME')}} with really fast reaction time! Thank you!</h3>
                <div class="d-flex align-items-center justify-content-center gap-3 mx-auto">
                  <div class="avatar avatar-3xl ">
                    <img class="rounded-circle border border-2 border-primary" src="{{ asset('images/prince.jpg')}}" alt="" />
                  </div>
                  <div class="text-start">
                    <h5>Johna Austin</h5>
                    <p class="mb-0">CEO</p>
                  </div>
                </div>
              </div>

            </div>
            <div class="carousel-indicators"><button class="active" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" aria-current="true" aria-label="Slide 1"></button><button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button><button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button></div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="pb-10 pb-xl-14">
        <div class="container-small px-lg-7 px-xxl-3">
          <div class="text-center mb-7">
            <h5 class="text-info mb-3">Contact</h5>
            <h2 class="mb-2">Choose the best deal for you</h2>
          </div>
          <div class="row">
            <div class="col-12 mb-10">
              <div class="googlemap" data-googlemap="data-googlemap" data-gmap="data-gmap" data-latlng="40.7228022,-74.0020158" data-scrollwheel="false" data-zoom="15" style="height: 381px; border-radius:1.5rem;">
                <div class="marker-content py-3">
                  <h5>Google map</h5>
                  <p>A nice template for your site.<br />Customize it as you want.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row g-5 g-lg-5">
            <div class="col-md-6 mb-5 mb-md-0 text-center text-md-start">
              <h3 class="mb-3">Stay connected</h3>
              <p class="mb-5">Stay connected with Phoenix's Help Center; Phoenix is available for your necessities at all times.</p>
              <div class="d-flex flex-column align-items-center align-items-md-start gap-3 gap-md-0">
                <div class="d-md-flex align-items-center">
                  <div class="icon-wrapper shadow-info"><span class="uil uil-phone text-primary light fs-4 z-index-1 ms-2"></span></div>
                  <div class="flex-1 ms-3"><a class="link-900" href="tel:+871406-7509">(265) 992823845</a></div>
                </div>
                <div class="d-md-flex align-items-center">
                  <div class="icon-wrapper shadow-info"><span class="uil uil-envelope text-primary light fs-4 z-index-1 ms-2"></span></div>
                  <div class="flex-1 ms-3"><a class="fw-semi-bold text-900" href="mailto:phoenix@email.com">princekennedy.developer@gmail.com</a></div>
                </div>
                <div class="mb-6 d-md-flex align-items-center">
                  <div class="icon-wrapper shadow-info"><span class="uil uil-map-marker text-primary light fs-4 z-index-1 ms-2"></span></div>
                  <div class="flex-1 ms-3"><a class="fw-semi-bold text-900" href="#!">Blantyre Malawi.</a></div>
                </div>
                <div class="d-flex"><a href="#!"><span class="fa-brands fa-facebook fs-2 text-primary me-3"></span></a><a href="#!"><span class="fa-brands fa-twitter fs-2 text-primary me-3"></span></a><a href="#!"><span class="fa-brands fa-linkedin-in fs-2 text-primary"></span></a></div>
              </div>
            </div>
            <div class="col-md-6 text-center text-md-start">
              <h3 class="mb-3">Drop us a line</h3>
              <p class="mb-7">If you have any query or suggestion , we are open to learn from you, Lets talk, reach us anytime.</p>
              <form class="row g-4">
                <div class="col-12"><input class="form-control bg-white" type="text" name="name" placeholder="Name" required="required" /></div>
                <div class="col-12"><input class="form-control bg-white" type="email" name="email" placeholder="Email" required="required" /></div>
                <div class="col-12"><textarea class="form-control bg-white" rows="6" name="message" placeholder="Message" required="required"></textarea></div>
                <div class="col-12 d-grid"><button class="btn btn-outline-primary" type="submit">Submit</button></div>
                <div class="feedback"></div>
              </form>
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->

      <section class="alternate-landing-team" id="team">
        <div class="position-absolute w-100 h-100 start-0 end-0 top-0 bg-soft-primary dark__bg-1000" style="transform: skewY(-6deg); transform-origin: right"></div>
        <div class="bg-holder d-none d-xl-block" style="background-image:url(../../assets/img/bg/bg-left-23.png);background-size:auto;background-position:left center;"></div>
        <!--/.bg-holder-->
        <div class="bg-holder d-none d-xl-block" style="background-image:url(../../assets/img/bg/bg-right-23.png);background-size:auto;background-position:right center;"></div>
        <!--/.bg-holder-->
        <div class="text-center mb-11 position-relative">
          <h5 class="text-info mb-3">Team</h5>
          <h2 class="mb-2">Our small team behind our success</h2>
        </div>
        <div class="container-small position-relative">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-6">
              <div class="row gx-3 gy-6 justify-content-center">
                <div class="col-sm-6 col-md-4">
                  <div class="text-center"> <img class="w-70 w-sm-100 rounded-4 mb-3" src="{{ asset('images/prince.jpg') }}" alt="" />
                    <h4>Prince Kennedy</h4>
                    <h5 class="fw-semi-bold">CEO</h5>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-soft-primary dark__bg-1000 pb-10 overflow-hidden">
        <div class="container-small px-lg-7 px-xxl-3">
          <div class="position-absolute w-100 h-100 start-0 end-0" style="bottom: -350px; transform: skewY(-8deg); background: linear-gradient(102.27deg, #38ABFF 4.69%, #3874FF 106.27%)"></div>
          <div class="bg-holder" style="background-image:url({{ asset('assets/img/bg/bg-left-24.png') }});background-size:auto;background-position:left center;"></div>
          <!--/.bg-holder-->
          <div class="bg-holder" style="background-image:url({{ asset('assets/img/bg/bg-right-24.png') }});background-size:auto;background-position:right center;"></div>
          <!--/.bg-holder-->
          <div class="row justify-content-center">
            <div class="col-12 text-center">
              <div class="card py-md-9 px-md-13 border-0 z-index-1 shadow-lg">
                <div class="bg-holder" style="background-image:url({{ asset('assets/img/bg/bg-38.png') }});background-position:center;background-size:100%;"></div>
                <!--/.bg-holder-->
                <div class="card-body position-relative">
                  <img class="img-fluid mb-5 d-dark-none" src="{{ asset('assets/img/spot-illustrations/37.png') }}" width="260" alt="..." />
                  <img class="img-fluid mb-5 d-light-none" src="{{ asset('assets/img/spot-illustrations/37_2.png') }}" width="260" alt="..." />
        
                  <h1 class="fs-2 fs-sm-4 fs-lg-6 fw-bolder lh-sm mb-3">
                    Join
                    <span class="gradient-text-primary mx-2">{{ env('APP_NAME')}}</span>
                    <span>Today</span>
                  </h1>
                  <form class="d-flex justify-content-center mb-3 px-xxl-15">
                    <div class="d-grid d-sm-block"></div>
                    <input class="form-control me-3" id="ctaEmail1" type="email" placeholder="Email" aria-describedby="ctaEmail1" />
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-1100 dark__bg-1000">
        <div class="container-small px-lg-7 px-xxl-3">
          <div class="row gx-xxl-8 gy-5 align-items-center mb-5">
            <div class="col-xl-auto text-center"><a href="#"><img src="../../assets/img/icons/logo-white.png" height="48" alt="" /></a></div>
            <div class="col-xl-auto flex-1">
              <ul class="list-unstyled d-flex justify-content-center flex-wrap mb-0 border-end-xl border-dashed border-800 gap-3 gap-xl-8 pe-xl-5 pe-xxl-8 w-75 w-md-100 mx-auto">
                <li><a class="text-300 dark__text-300" href="#">Contact us</a></li>
                <li><a class="text-300 dark__text-300" href="#">Newsroom</a></li>
                <li><a class="text-300 dark__text-300" href="#">Opportunities</a></li>
                <li><a class="text-300 dark__text-300" href="#">Login</a></li>
                <li><a class="text-300 dark__text-300" href="#">Sign Up</a></li>
                <li><a class="text-300 dark__text-300" href="#">Support</a></li>
                <li><a class="text-300 dark__text-300" href="#">FAQ</a></li>
              </ul>
            </div>
            <div class="col-xl-auto">
              <div class="d-flex align-items-center justify-content-center gap-8"><a class="text-white dark__text-white" href="#!"> <span class="fa-brands fa-facebook"></span></a><a class="text-white dark__text-white" href="#!"> <span class="fa-brands fa-twitter"></span></a><a class="text-white dark__text-white" href="#!"> <span class="fa-brands fa-linkedin-in"></span></a></div>
            </div>
          </div>
          <hr class="text-800" />
          <div class="d-sm-flex flex-between-center text-center">
            <p class="text-600 mb-0">Copyright © Company Name</p>
            <p class="text-600 mb-0">Made with love by <a href="https://themewagon.com/">ThemeWagon</a></p>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->

@endsection   