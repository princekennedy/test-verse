@extends('layouts.layout')

@section('content')

<div class="">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                Profile
            </h4>

            <x-form
                method="POST"
                action="/profile/{{ auth()->id() }}"
                has-files
                class="mt-4"
            >
                @php $editing = isset($user) @endphp

                <div class="row">
                    <x-inputs.group class="col-sm-12">
                        <x-inputs.text
                            name="name"
                            label="Name"
                            :value="old('name', ($editing ? $user->name : ''))"
                            maxlength="255"
                            placeholder="Name"
                            required
                        ></x-inputs.text>
                    </x-inputs.group>

                    <x-inputs.group class="col-sm-12">
                        <x-inputs.email
                            name="email"
                            label="Email"
                            :value="old('email', ($editing ? $user->email : ''))"
                            maxlength="255"
                            placeholder="Email"
                            required
                        ></x-inputs.email>
                    </x-inputs.group>

                    <x-inputs.group class="col-sm-12">
                        <x-inputs.password
                            name="password"
                            label="Password"
                            maxlength="255"
                            placeholder="Password"
                            :required="!$editing"
                        ></x-inputs.password>
                    </x-inputs.group>

                    <x-inputs.group class="col-sm-12">
                        <x-inputs.text
                            name="phone"
                            label="Phone"
                            :value="old('phone', ($editing ? $user->phone : ''))"
                            maxlength="255"
                            placeholder="Phone"
                        ></x-inputs.text>
                    </x-inputs.group>

                    <x-inputs.group class="col-sm-12">
                        <div
                            x-data="imageViewer('{{ $editing ? $user->image : '' }}')"
                        >
                            <x-inputs.partials.label
                                name="image"
                                label="Image"
                            ></x-inputs.partials.label
                            ><br />

                            <!-- Show the image -->
                            <template x-if="imageUrl">
                                <img
                                    :src="imageUrl"
                                    class="object-cover rounded border border-gray-200"
                                    style="width: 100px; height: 100px;"
                                />
                            </template>

                            <!-- Show the gray box when image is not available -->
                            <template x-if="!imageUrl">
                                <div
                                    class="border rounded border-gray-200 bg-gray-100"
                                    style="width: 100px; height: 100px;"
                                ></div>
                            </template>

                            <div class="mt-2">
                                <input
                                    type="file"
                                    name="image"
                                    id="image"
                                    @change="fileChosen"
                                />
                            </div>

                            @error('image') @include('components.inputs.partials.error')
                            @enderror
                        </div>
                    </x-inputs.group>

                </div>

                <div class="mt-4">
                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.update')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection
