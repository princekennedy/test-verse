@extends('layouts.guest')

@section('content')
<form action="{{ route('register') }}" method="POST" class="container">
@csrf
    <div class="row flex-center min-vh-100 py-5">
        <div class="col-sm-10 col-md-8 col-lg-5 col-xl-5 col-xxl-3">
            <a class="d-flex flex-center text-decoration-none mb-4" href="/">
                <div class="d-flex align-items-center fw-bolder fs-5 d-inline-block">
                    <img src="{{ asset('img/logo.PNG') }}" alt="phoenix" width="58" />
                </div>
            </a>

            @include('partial.messages')
            <div class="auth-form-box">
<!--                 <button type="button" class="btn btn-phoenix-secondary w-100 mb-3">
                    <span class="fab fa-google text-danger me-2 fs--1"></span>Sign in with google
                </button>
                <button type="button" class="btn btn-phoenix-secondary w-100">
                    <span class="fab fa-facebook text-primary me-2 fs--1"></span>Sign in with facebook
                </button>
                <div class="position-relative">
                  <hr class="bg-200 mt-5 mb-4" />
                  <div class="divider-content-center bg-white">or use email</div>
                </div> -->
                    
                <div class="mb-3 text-start">
                    <label class="form-label" for="name">Name</label>
                    <input class="form-control" name="name" id="name" type="text" placeholder="Name" required />
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>    

                <div class="mb-3 text-start">
                    <label class="form-label" for="name">Phone</label>
                    <input class="form-control" name="phone" id="phone" type="text" placeholder="Phone" required />
                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>


                <div class="mb-3 text-start">
                    <label class="form-label" for="email">Email address</label>
                    <input class="form-control" name="email" id="email" type="email" placeholder="name@example.com" required />
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror               
                </div>
                <div class="row g-3 mb-3">
                <div class="col-xl-6">
                    <label class="form-label" for="password">Password</label>
                    <input class="form-control form-icon-input" name="password" id="password" type="password" placeholder="Password" />
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-xl-6">
                    <label class="form-label" for="confirmPassword">Confirm Password</label>
                    <input name="password_confirmation" class="form-control form-icon-input" id="confirmPassword" type="password" placeholder="Confirm Password" />
                </div>
                </div>
                <div class="form-check mb-3">
                <input class="form-check-input" id="termsService" type="checkbox" />
                <label class="form-label fs--1 text-none" for="termsService">I accept the 
                  <a href="#!">terms </a>and 
                  <a href="#!">privacy policy</a>
                </label>
                </div>

                <button type="submit" class="btn btn-primary w-100 mb-3">Sign Up</button>
                <div class="text-center"><a class="fs--1 fw-bold" href="/login">Sign In</a></div>
            </div>


        </div>
    </div>
</form>


@endsection

