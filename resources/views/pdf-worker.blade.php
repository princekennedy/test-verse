<!DOCTYPE html>
<html>
  <head></head>

  <body>
    <script src="{{ asset('pdf-worker/pdf.min.js') }}"></script>
    <script>
      pdfjsLib.GlobalWorkerOptions.workerSrc = "{{ asset('/pdf-worker/pdf.worker.min.js') }}";
      function extractText(pdfUrl) {
        var pdf = pdfjsLib.getDocument(pdfUrl);
        return pdf.promise.then(function (pdf) {
          var totalPageCount = pdf.numPages;
          console.log(totalPageCount)
          var countPromises = [];
          for (
            var currentPage = 1;
            currentPage <= totalPageCount;
            currentPage++
          ) {
            var page = pdf.getPage(currentPage);
            countPromises.push(
              page.then(function (page) {
                var textContent = page.getTextContent();
                console.log(page.then)
                return textContent.then(function (text) {
                  return text.items
                    .map(function (s) {
                      return s.str;
                    })
                    .join('');
                });
              }),
            );
          }
          return Promise.all(countPromises).then(function (texts) {
            return texts.join('');
          });
        });
      }

      var url = '/doc/two.pdf';

      extractText(url).then(
        function (text) {
          if(text) {
            console.log("PDF Content:");
            console.log(text);
          } else {
            console.log("PDF Unreadable:");
          }
        },
        function (reason) {
          console.error(reason);
        },
      );
    </script>
  </body>
</html>