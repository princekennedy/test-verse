<div>
    <h1>Maximum file size 20MB.</h1>   
    <form wire:submit.prevent="submit">
        {{ $this->form }}
         @if ($this->image)
        <span class="inline-flex rounded-md shadow-sm mt-4">
            <button
                type="submit"
                class="
                    inline-flex
                    items-center
                    px-4
                    py-2
                    text-base
                    font-medium
                    leading-6
                    text-white
                    transition
                    duration-150
                    ease-in-out
                    bg-primary-600
                    border border-transparent
                    rounded-md
                    hover:bg-blue-500
                    focus:border-blue-700
                    active:bg-blue-700
                "
            >
                <svg
                    wire:loading
                    class="w-5 h-5 mr-3 -ml-1 text-white animate-spin"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <circle
                        class="opacity-25"
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        stroke-width="4"
                    ></circle>
                    <path
                        class="opacity-75"
                        fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                    ></path>
                </svg>
                Optimize
            </button>
        </span>


         @endif
    </form>

    @if( $this->optimiser )

        <div class="mt-4">
            <button wire:click="download('{{ $this->optimiser->file()->getUrl() }}')" class="mt-4 bg-white hover:bg-primary text-primary font-semibold py-2 px-4 border border-primary rounded shadow">
                <svg
                    wire:loading
                    class="w-5 h-5 mr-3 -ml-1 text-black animate-spin"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <circle
                        class="opacity-25"
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        stroke-width="4"
                    ></circle>
                    <path
                        class="opacity-75"
                        fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                    ></path>
                </svg>
                  Download Original
                  <span class="inline-flex items-center justify-center h-4 ml-2 text-xs font-semibold text-primary bg-primary rounded-full">
                    size: {{ _formatSizeUnits($this->optimiser->file()->size ) }}
                  </span>
            </button>              
            <button wire:click="download('{{ $this->optimiser->optimised }}')" class="mt-4 bg-white hover:bg-primary text-primary font-semibold py-2 px-4 border border-primary rounded shadow">
                <svg
                    wire:loading
                    class="w-5 h-5 mr-3 -ml-1 text-black animate-spin"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <circle
                        class="opacity-25"
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        stroke-width="4"
                    ></circle>
                    <path
                        class="opacity-75"
                        fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                    ></path>
                </svg>
                  Download Optimised
                  <span class="inline-flex items-center justify-center h-4 ml-2 text-xs font-semibold text-primary bg-primary rounded-full">
                    size: {{ _formatSizeUnits($this->optimiser->file()->size / 3 ) }}
                  </span>
            </button>    
        </div>
    @endif

    @if( $this->result )
        <div class="mt-4">
            <button wire:click="download('{{ _from($this->result, 'src') }}')" class="mt-4 bg-white hover:bg-primary text-primary font-semibold py-2 px-4 border border-primary rounded shadow">
                <svg
                    wire:loading
                    class="w-5 h-5 mr-3 -ml-1 text-black animate-spin"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <circle
                        class="opacity-25"
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        stroke-width="4"
                    ></circle>
                    <path
                        class="opacity-75"
                        fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                    ></path>
                </svg>
                  Download Original
                  <span class="inline-flex items-center justify-center h-4 ml-2 text-xs font-semibold text-primary bg-primary rounded-full">
                    size: {{ _formatSizeUnits(_from($this->result, 'src_size')) }}
                  </span>
            </button>    
            <button wire:click="download('{{ _from($this->result, 'dest') }}')" class="mt-4 bg-white hover:bg-primary text-primary font-semibold py-2 px-4 border border-primary rounded shadow">

                <svg
                    wire:loading
                    class="w-5 h-5 mr-3 -ml-1 text-black animate-spin"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <circle
                        class="opacity-25"
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        stroke-width="4"
                    ></circle>
                    <path
                        class="opacity-75"
                        fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                    ></path>
                </svg>
                  Download Optimised
                  <span class="inline-flex items-center justify-center h-4 ml-2 text-xs font-semibold text-primary bg-primary rounded-full">
                    size: {{ _formatSizeUnits(_from($this->result, 'dest_size')) }}
                  </span>
            </button>
        </div>
    @endif

</div>


@push('scripts')

    <script type="text/javascript">
        window.livewire.on('download', url => {
            alert(url);

        });
    </script>

@endpush