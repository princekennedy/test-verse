<div id="chat-el">

    <div class="container mx-auto" style="margin-top: -50px;">
        <div class="py-6 h-screen">
            <div class="flex ">

                <!-- Right -->
                <div class="w-full border flex flex-col">

                    <!-- Header -->
                    <div class="py-2 px-3 bg-grey-lighter flex flex-row justify-between items-center">
                        <div class="flex items-center">
                            <div>
                                <img class="w-10 h-10 rounded-full" src="{{ auth()->user()->image}}"/>
                            </div>
                            <div class="ml-4">
                                <p class="text-grey-darkest">
                                    Machine
                                </p>
                                <p class="text-grey-darker text-xs mt-1">
                                    chat with machine
                                </p>
                            </div>
                        </div>

                        <div class="flex">
                            <div class="ml-6 cursor-pointer" >
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                    <path fill="#263238" fill-opacity=".6" d="M12 7a2 2 0 1 0-.001-4.001A2 2 0 0 0 12 7zm0 2a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 9zm0 6a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 15z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <!-- Messages -->
                    <div class="flex-1 overflow-auto" style="background-color: #DAD3CC;">
                        <div class="py-2 px-3" id="messages" style=" overflow: auto; height: 65vh;">

                            @foreach($this->messages as $message)
                            @if( _from( $message, 'me' ) )

                            <div class="flex justify-end mb-2">
                                <div class="rounded py-2 px-3" style="background-color: #E2F7CB">
                                    <p class="text-sm mt-1">
                                        {!! _from($message, 'text') ?? '' !!}
                                    </p>
                                    <p class="text-right text-xs text-grey-dark mt-1">
                                        {{ _from($message, 'dateTime') ?? ''}}
                                    </p>
                                </div>
                            </div>

                            @else

                            <div class="flex mb-2">
                                <div class="rounded py-2 px-3" style="background-color: #F2F2F2">
                                    <p class="text-sm text-purple">
                                        {!! _from($message, 'name') ?? '' !!}
                                    </p>
                                    <p class="text-sm mt-1">
                                        {!! _from($message, 'text') ?? '' !!}
                                    </p>
                                    <p class="text-right text-xs text-grey-dark mt-1">
                                        {{ _from($message, 'dateTime') ?? ''}}
                                    </p>
                                </div>
                            </div>

                            @endif
                            @endforeach

                            <div class="flex justify-end mb-2">
                                <div  wire:loading class="rounded py-2 px-3" style="background-color: #E2F7CB">
                                    <p class="text-sm mt-1">
                                        {!! $this->search !!}
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Input -->
                    <form wire:submit.prevent="submit" class="bg-grey-lighter px-4 py-4 flex items-center">
                        <div class="flex-1 mx-4">
                            <input name="search" id="search" wire:model="search"  class="w-full border rounded px-2 py-2" type="text"/>
                        </div>
                        <div  class="p-2 ml-1">
                            <x-filament::button type="submit">
                                Send <span wire:loading>...</span>
                            </x-filament::button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


@push("scripts")

    <script type="text/javascript">

        setTimeout(() => {
            const style = document.createElement('style');
            style.innerHTML = `
                  .filament-body {
                    overflow:hidden;
                  }
                `;
            document.head.appendChild(style);
        },1000)

        window.addEventListener('scrollToBottom', (e) => {
            var id = e.detail.id;
            const element = document.getElementById(id);
            element.scrollTop = element.scrollHeight;
        });

        function removeElementsByClass(className){
            const elements = document.getElementsByClassName(className);
            while(elements.length > 0){
                elements[0].parentNode.removeChild(elements[0]);
            }
        }
        removeElementsByClass('filament-header-heading');

    </script>
@endpush