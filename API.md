
# API Documentation: Image Optimization Endpoint

## Endpoint

`POST /13.112.159.92:100`

`
{
	image: Image File
}
`

This API endpoint allows the user to upload an image file and receive a response containing information about the optimized version of the uploaded image.

## Parameters

- `image`: This parameter accepts a file object containing the image to be optimized.

## Response

Upon successful completion of the API request, the response object will contain the following properties:

- `client_type`: This property will be null in the response.
- `src`: This property will contain the URL of the original image uploaded by the user.
- `dest`: This property will contain the URL of the optimized version of the uploaded image.
- `src_size`: This property will contain the size of the original image in bytes.
- `dest_size`: This property will contain the size of the optimized image in bytes.
- `percent`: This property will contain the percentage reduction achieved in the image size through optimization.
- `output`: This property will contain the format of the response, which is in JSON format.
- `expires`: This property will contain the date and time at which the response will expire.
- `generator`: This property will contain the version of the image optimization software used to optimize the image.

Note: The response object may contain additional properties depending on the specific implementation of the image optimization software used by the API endpoint.

## Example Request

